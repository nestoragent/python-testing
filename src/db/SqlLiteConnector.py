import sqlite3


class SqlLiteConnector(object):

    def get_client_id(self):

        conn = sqlite3.connect('/home/sbt-velichko-aa/git/testing/web/clients.db')

        print "Opened database successfully";
        cursor = conn.execute("select * from main.clients c INNER JOIN main.balances b ON c.CLIENT_ID = b.CLIENTS_CLIENT_ID where b.BALANCE > 1")
        if (len(cursor.fetchall()) == 0):
            # Insert a row of data
            conn.execute("insert into clients (CLIENT_NAME) values ('testAccount')")
            conn.commit()
            cursor = conn.execute("select CLIENT_ID from clients where CLIENT_NAME = 'testAccount' order by CLIENT_ID desc")
            # Save (commit) the changes
            for row in cursor:
                clientId = row[0]
                break
            client = (clientId, 5)
            conn.execute("insert into balances (CLIENTS_CLIENT_ID, BALANCE) values (?,?)", client);
            conn.commit()
        else:
            cursor = conn.execute("select * from main.clients c INNER JOIN main.balances b ON c.CLIENT_ID = b.CLIENTS_CLIENT_ID where b.BALANCE > 1")
            for row in cursor:
                clientId = row[0]
                break

        print "Operation done successfully";
        conn.close()
        print "clientId: ", clientId
        return clientId;

    def get_balance(self, clientId):
        print "clientId come: ", clientId
        conn = sqlite3.connect('/home/sbt-velichko-aa/git/testing/web/clients.db')
        print "Opened database successfully";
        cursor = conn.execute("select BALANCE from balances where CLIENTS_CLIENT_ID = '%s'" % clientId)
        for row in cursor:
            balance = row[0]
            break
        print "balance return: ", balance
        return balance;
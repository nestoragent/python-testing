import json
import RequestsLibrary.RequestsKeywords
import time
import unittest

class StepDefs(object):

    def get_new_service(self, added_services, all_services):
        str_added_services = str(added_services)
        str_all_services = str(all_services)
        parsed_added_services = json.loads(str_added_services.replace('u', '', 100).replace("'", "\"", 10000))
        print "load done"
        print "parsed_added_services:", parsed_added_services['items']
        parsed_all_services = json.loads(str_all_services.replace('u', '', 100).replace("'", "\"", 10000))
        print "parsed_all_services:", parsed_all_services['items']
        for all in parsed_all_services['items']:
            print "id: ", all['id']
            print "cost: ", all['cost']
            if "id': " + str(all['id']) not in str_added_services:
                serviceId = all['id']
                print 'success'
                break
        return serviceId;

    def get_service_cost(self, all_services, serviceId):
        print "all_services:", all_services
        str_all_services = str(all_services)
        parsed_all_services = json.loads(str_all_services.replace('u', '', 100).replace("'", "\"", 10000))
        for all in parsed_all_services['items']:
            print "id: ", all['id']
            print "cost: ", all['cost']
            if (int(all['id']) == int(serviceId)):
                cost = all['cost']
                print 'success'
                break
        return cost

    def check_service_added(self, alias, serviceId, data, uri, headers):
        print "alias:", alias
        print "data:", data
        print "serviceId:", serviceId

        request = RequestsLibrary.RequestsKeywords()
        request.create_session(alias=alias, url='http://localhost:5000')
        print "done alias"

        exist = False
        i = 0;
        while i < 60:
            response = request.post_request(alias=alias, uri=uri, data=data, headers=headers)
            if ("id\": " + str(serviceId) in response.content):
                exist = True
                break
            time.sleep(1)
            i += 1;

        if not exist:
            raise AssertionError("1 min end. Service didn't added to user.")

        response = request.post_request(alias=alias, uri=uri, data=data, headers=headers)
        print "code: ", response.status_code
        print "content: ", response.content
        return response

    def equals_balances(self, balanceStart, balanceEnd, addedServiceCost):
        print "balanceStart", balanceStart
        print "balanceEnd", balanceEnd
        print "addedServiceCost", addedServiceCost
        if not int(balanceEnd) == (int(balanceStart) - int(addedServiceCost)):
            raise AssertionError("End balance didn't equals start balance - added service cost.")
